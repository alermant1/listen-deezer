package com.example.userinsta.mydeezer.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by userinsta on 11/04/2017.
 */

public class Music implements Serializable{

    private String title;
    private String artist;
    private String album;
    private boolean isFavorite;
    private String sampleUrl;
    private String link;
    private String coverUrl;

    public Music() {
    }

    public Music(String title, String artist, String album, boolean isFavorite, String sampleUrl, String link, String coverUrl) {
        this.title = title;
        this.artist = artist;
        this.album = album;
        this.isFavorite = isFavorite;
        this.sampleUrl = sampleUrl;
        this.link = link;
        this.coverUrl = coverUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public String getSampleUrl() {
        return sampleUrl;
    }

    public void setSampleUrl(String sampleUrl) {
        this.sampleUrl = sampleUrl;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public static ArrayList<Music> getAllMusics(int n){
        ArrayList<Music> allMusics = new ArrayList<Music>(n);
        for(int i=0; i<n; i++){
            int track= i+1;
            allMusics.add(i, new Music("track - "+track, "Unknown Artist",
                        "Unknown Album", false, "", "", ""));
        }
        return allMusics;
    }

    public static Music getDefaultMusic(){
        return new Music("Billie jean", "Michael Jackson", "Thriller", false,
                "",
                "https://open.spotify.com/artist/3fMbdgg4jU18AjLCKBhRSm",
                "https://i.scdn.co/image/bceb8dcb7cbd0d703392f66f34a940dee8fc1ee9");
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Music){
            Music tmp = (Music) obj;
            return artist.equals(tmp.getArtist())
                    && album.equals(tmp.getAlbum())
                    && title.equals(tmp.getTitle());
        }
        return false;
    }
}

package com.example.userinsta.mydeezer.model;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.userinsta.mydeezer.R;
import android.R.drawable;

import java.util.ArrayList;

/**
 * Created by userinsta on 11/04/2017.
 */

public class MusicAdapter extends BaseAdapter{

    private Activity context;
    private ArrayList<Music> musics;

    public MusicAdapter(Activity context, ArrayList<Music> musics) {
        this.context = context;
        this.musics = musics;
    }

    @Override
    public int getCount() {
        return musics.size();
    }

    @Override
    public Object getItem(int position) {
        return musics.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.details_layout, null);
        ImageView imageViewAlbum = (ImageView) rowView.findViewById(R.id.ImageViewCoverAlbum);
        TextView textViewMusic = (TextView) rowView.findViewById(R.id.textViewListAlbum);
        ImageView imageViewFavoris = (ImageView) rowView.findViewById(R.id.ImageViewFavoris);

        textViewMusic.setText(musics.get(position).getArtist()+"\n"+musics.get(position).getTitle());
        if(musics.get(position).isFavorite()){
            imageViewFavoris.setImageResource(drawable.star_big_on);
        }else{
            imageViewFavoris.setImageResource(drawable.star_big_off);
        }

        DownloadImagesTask imagesTask= new DownloadImagesTask(imageViewAlbum);
        imagesTask.execute(musics.get(position).getCoverUrl());
        return rowView;
    }
}

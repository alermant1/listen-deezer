package com.example.userinsta.mydeezer.model;

/**
 * Created by userinsta on 12/04/2017.
 */

public interface IOnFavoriteChange {

    public void onFavoriteChange(Music m, boolean isFavorite);
}

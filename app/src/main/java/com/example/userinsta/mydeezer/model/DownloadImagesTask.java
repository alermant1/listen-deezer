package com.example.userinsta.mydeezer.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by userinsta on 11/04/2017.
 */

public class DownloadImagesTask extends AsyncTask<String, Void, Bitmap>{
    ImageView myImageView;

    public DownloadImagesTask(ImageView myImageView) {
        this.myImageView = myImageView;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        return download_image(params[0]);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        myImageView.setImageBitmap(bitmap);
        myImageView.invalidate();
    }

    private Bitmap download_image(String url){
        Bitmap bm = null;
        try {
            URL aUrl = new URL(url);
            URLConnection conn= aUrl.openConnection();
            conn.connect();
            InputStream is = conn.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            bm = BitmapFactory.decodeStream(bis);
            bis.close();
            is.close();
        } catch (IOException e) {
            Log.e("MyDeezer", "Error getting image : "+e.getMessage().toString());
        }
        return bm;
    }
}

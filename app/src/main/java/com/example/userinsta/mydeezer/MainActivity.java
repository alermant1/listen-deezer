package com.example.userinsta.mydeezer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.example.userinsta.mydeezer.model.DownloadImagesTask;
import com.example.userinsta.mydeezer.model.DownloadJsonTask;
import com.example.userinsta.mydeezer.model.IOnFavoriteChange;
import com.example.userinsta.mydeezer.model.ManageFavorites;
import com.example.userinsta.mydeezer.model.Music;
import com.example.userinsta.mydeezer.model.MusicAdapter;

import java.util.ArrayList;

public class MainActivity extends Activity {

    private static final int ACTION_SELECT = 1;
    private static final int ACTION_FAV_ON = 2;
    private static final int ACTION_FAV_OFF= 3;

    private EditText searchText;
    private ImageButton searchButton;
    private ListView listViewMusics;

    private ArrayList<Music> musics;
    private MusicAdapter adapter;

    private static final String URL_MUSIC_SEARCH = "https://itunes.apple.com/search?term=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        searchText = (EditText) findViewById(R.id.editTextSearch);
        searchText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER){
                    if(!searchText.getText().toString().equals("")){
                        searchMusics(searchText.getText().toString());
                    }
                    return true;
                }
                return false;
            }
        });

        searchButton = (ImageButton) findViewById(R.id.imageButtonSearch);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!searchText.getText().toString().equals("")){
                    searchMusics(searchText.getText().toString());
                }
            }
        });


        musics = new ArrayList<Music>();//Music.getAllMusics(10);
        //musics.add(Music.getDefaultMusic());

        listViewMusics = (ListView) findViewById(R.id.listViewMainSearch);
        adapter = new MusicAdapter(this, musics);
        listViewMusics.setAdapter(adapter);
        listViewMusics.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                goToMusic(position);
            }
        });
        listViewMusics.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                menu.add(Menu.NONE, ACTION_SELECT, 0, "Selectionner");
                AdapterView.AdapterContextMenuInfo adapterContext = (AdapterView.AdapterContextMenuInfo) menuInfo;
                menu.setHeaderTitle(musics.get(adapterContext.position).getTitle());
                if(musics.get(adapterContext.position).isFavorite()){
                    menu.add(Menu.NONE, ACTION_FAV_OFF, 1, "Retirer des favoris");
                }else{
                    menu.add(Menu.NONE, ACTION_FAV_ON, 1, "Ajouter des favoris");
                }
            }
        });

        ManageFavorites.setListener(new IOnFavoriteChange() {
            @Override
            public void onFavoriteChange(Music m, boolean isFavorite) {
                musics.get(musics.indexOf(m)).setFavorite(isFavorite);
                ((BaseAdapter)listViewMusics.getAdapter()).notifyDataSetChanged();
            }
        });
    }

    public void goToMusic(int position){
        Intent musicIntent = new Intent(getApplicationContext(), MusicActivity.class);
        musicIntent.putExtra("musicSelected", musics.get(position));
        startActivity(musicIntent);
    }

    private void searchMusics(String textSearch){
        final ProgressDialog spinner = new ProgressDialog(this);
        spinner.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        spinner.setTitle("Recherche : "+ textSearch);
        spinner.setMessage("Chargement en cours ...");
        spinner.setCancelable(false);
        spinner.show();

        String url= URL_MUSIC_SEARCH+ textSearch.replace(" ", "+");
        DownloadJsonTask jsonTask = new DownloadJsonTask(this, musics, listViewMusics, spinner);
        jsonTask.execute(url);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()){
            case ACTION_SELECT:
                goToMusic(info.position);
                break;
            case ACTION_FAV_ON:
                musics.get(info.position).setFavorite(true);
                ManageFavorites.add(this, musics.get(info.position));
                break;
            case ACTION_FAV_OFF:
                ManageFavorites.remove(this, musics.get(info.position));
                musics.get(info.position).setFavorite(false);
                break;
        }
        ((BaseAdapter) listViewMusics.getAdapter()).notifyDataSetChanged();
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id= item.getItemId();
        if(id == R.id.action_favorites){
            musics = ManageFavorites.load(this);
            adapter= new MusicAdapter(this, musics);
            listViewMusics.setAdapter(adapter);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

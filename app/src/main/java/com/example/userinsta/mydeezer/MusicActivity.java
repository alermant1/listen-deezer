package com.example.userinsta.mydeezer;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.userinsta.mydeezer.model.DownloadImagesTask;
import com.example.userinsta.mydeezer.model.ManageFavorites;
import com.example.userinsta.mydeezer.model.Music;

import java.io.IOException;

/**
 * Created by userinsta on 11/04/2017.
 */

public class MusicActivity extends Activity {

    TextView fieldTitle, fieldArtist, fieldAlbum;
    RadioButton buttonFavYes, buttonFavNo;
    ImageView coverMusic;

    private MediaPlayer player;
    private Music currentMusic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.music_layout);

        fieldTitle = (TextView) findViewById(R.id.textViewMusicTitle);
        fieldArtist= (TextView) findViewById(R.id.textViewMusicArtist);
        fieldAlbum = (TextView) findViewById(R.id.textViewMusicAlbum);
        buttonFavYes= (RadioButton) findViewById(R.id.radioButtonFavYes);
        buttonFavYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentMusic.setFavorite(true);
                ManageFavorites.add(getApplicationContext(), currentMusic);
            }
        });

        buttonFavNo = (RadioButton) findViewById(R.id.radioButtonFavNo);
        buttonFavNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentMusic.setFavorite(false);
                ManageFavorites.remove(getApplicationContext(), currentMusic);
            }
        });

        coverMusic  = (ImageView) findViewById(R.id.imageViewMusicCover);

        Button buttonLink = (Button) findViewById(R.id.buttonMusicDeezer);
        buttonLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchWebBrowser();
            }
        });

        player = new MediaPlayer();
        Button buttonPlay = (Button) findViewById(R.id.buttonMusicListen);
        buttonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play();
            }
        });
    }

    public void setSelectedMusic(Music m){
        currentMusic= m;
        fieldTitle.setText(m.getTitle());
        fieldArtist.setText(m.getArtist());
        fieldAlbum.setText(m.getAlbum());
        DownloadImagesTask imagesTask = new DownloadImagesTask(coverMusic);
        imagesTask.execute(m.getCoverUrl());
        if(m.isFavorite()){
            buttonFavYes.setChecked(true);
        }else{
            buttonFavNo.setChecked(true);
        }
    }

    private void launchWebBrowser(){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(currentMusic.getLink()));
        startActivity(intent);
    }

    private void play(){
        try {
            if(!player.isPlaying()){
                player.reset();
                player.setDataSource(this, Uri.parse(this.currentMusic.getSampleUrl()));
                player.prepare();
                player.start();
            }else{
                player.stop();
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent myIntent= getIntent();
        Music music = (Music) myIntent.getSerializableExtra("musicSelected");
        setSelectedMusic(music);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            player.stop();
        }
        return super.onKeyDown(keyCode, event);
    }
}

package com.example.userinsta.mydeezer.model;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.example.userinsta.mydeezer.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by userinsta on 11/04/2017.
 */

public class DownloadJsonTask extends AsyncTask<String, Void, String>{
    private ArrayList<Music> musics;
    private ListView listViewMusics;
    private ProgressDialog spinner;
    private Context context;

    public DownloadJsonTask(Context context, ArrayList<Music> musics, ListView listViewMusics, ProgressDialog spinner) {
        this.musics = musics;
        this.listViewMusics = listViewMusics;
        this.spinner = spinner;
        this.context = context;
    }

    @Override
    protected String doInBackground(String... params) {
        return download_xmldom(params[0]);
    }

    @Override
    protected void onPostExecute(String result) {
        ituneJsonToMusics(result);
        //deezerJsonToMusics(result);
    }

    private void ituneJsonToMusics(String result){
        musics.clear();
        try {
            JSONObject jsonObject= new JSONObject(result);
            JSONArray tracks= jsonObject.getJSONArray("results");
            for(int i=0; i< tracks.length(); i++){
                JSONObject track = tracks.getJSONObject(i);
                if(track.getString("kind").equals("song")) {
                    Music m = new Music();
                    m.setTitle(track.getString("trackName"));
                    m.setArtist(track.getString("artistName"));
                    m.setAlbum(track.getString("collectionName"));
                    m.setFavorite(true);
                    m.setSampleUrl(track.getString("previewUrl"));
                    m.setLink(track.getString("trackViewUrl"));
                    m.setCoverUrl(track.getString("artworkUrl100"));
                    if(!ManageFavorites.isFavorites(context, m)){
                        m.setFavorite(false);
                    }
                    musics.add(m);
                    ((BaseAdapter) listViewMusics.getAdapter()).notifyDataSetChanged();
                }
            }
            spinner.dismiss();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void deezerJsonToMusics(String result){
        musics.clear();
        try {
            JSONObject jsonObject= new JSONObject(result);
            JSONArray tracks= jsonObject.getJSONArray("data");
            for(int i=0; i< tracks.length(); i++){
                JSONObject track = tracks.getJSONObject(i);
                Music m = new Music();
                m.setTitle(track.getString("title"));
                m.setArtist(track.getJSONObject("artist").getString("name"));
                m.setAlbum(track.getJSONObject("album").getString("name"));
                m.setFavorite(false);
                m.setSampleUrl(track.getString("preview"));
                m.setLink(track.getString("link"));
                m.setCoverUrl(track.getJSONObject("album").getString("cover"));
                musics.add(m);
                ((BaseAdapter) listViewMusics.getAdapter()).notifyDataSetChanged();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String download_xmldom(String url){
        String result="";
        try {
            URL aUrl = new URL(url);
            URLConnection conn= aUrl.openConnection();
            conn.connect();
            InputStream is = conn.getInputStream();
            /*BufferedInputStream bis = new BufferedInputStream(is);
            byte[] contents = new byte[1024];
            int bytesRead = 0;
            String strContent="";
            while((bytesRead = bis.read(contents))!=-1){
                strContent += new String(contents, 0, bytesRead);
            }
            result = strContent;*/
            result= convertStreamToString(is);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private String convertStreamToString(InputStream is){
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try{
            while((line = reader.readLine()) != null){
                sb.append(line).append('\n');
            }
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try {
                is.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
